package ru.tsc.kitaev.tm.command.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kitaev.tm.dto.Domain;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

public class BackupLoadCommand extends AbstractDataCommand {

    @NotNull
    public final static String BACKUP_LOAD = "backup-load";

    @NotNull
    @Override
    public String name() {
        return BACKUP_LOAD;
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Load backup data.";
    }

    @SneakyThrows
    @Override
    public void execute() {
        @NotNull File file = new File(BACKUP_XML);
        if (!file.exists()) return;
        @NotNull final String xml = new String(Files.readAllBytes(Paths.get(BACKUP_XML)));
        @NotNull final ObjectMapper objectMapper = new XmlMapper();
        @NotNull final Domain domain = objectMapper.readValue(xml, Domain.class);
        setDomain(domain);
    }

}
