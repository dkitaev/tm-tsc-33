package ru.tsc.kitaev.tm.component;

import org.jetbrains.annotations.NotNull;
import ru.tsc.kitaev.tm.command.data.BackupLoadCommand;
import ru.tsc.kitaev.tm.command.data.BackupSaveCommand;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class Backup {

    @NotNull
    private final ScheduledExecutorService es = Executors.newSingleThreadScheduledExecutor();

    @NotNull
    private final Bootstrap bootstrap;

    private static final int INTERVAL = 30;

    public Backup(@NotNull final Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    public void init() {
        load();
        es.scheduleWithFixedDelay(this::save, 0, INTERVAL, TimeUnit.SECONDS);
    }

    public void stop() {
        es.shutdown();
    }

    public void save() {
        bootstrap.runCommand(BackupSaveCommand.BACKUP_SAVE);
    }

    public void load() {
        bootstrap.runCommand(BackupLoadCommand.BACKUP_LOAD);
    }

}
