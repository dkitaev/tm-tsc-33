package ru.tsc.kitaev.tm.model;

import lombok.Getter;
import org.jetbrains.annotations.Nullable;

@Getter
public final class Command {

    @Nullable
    private final String name;

    @Nullable
    private final String argument;

    @Nullable
    private final String description;

    public Command(@Nullable String name, @Nullable String argument, @Nullable String description) {
        this.name = name;
        this.argument = argument;
        this.description = description;
    }

    @Override
    public String toString() {
        String result = "";
        if (name != null && !name.isEmpty()) result += name + " ";
        if (argument != null && !argument.isEmpty()) result += "(" + argument + ") ";
        if (description != null && !description.isEmpty()) result += description;
        return result;
    }

}
