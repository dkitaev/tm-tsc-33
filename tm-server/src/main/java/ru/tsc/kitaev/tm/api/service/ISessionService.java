package ru.tsc.kitaev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kitaev.tm.enumerated.Role;
import ru.tsc.kitaev.tm.model.Session;
import ru.tsc.kitaev.tm.model.User;

public interface ISessionService extends IService<Session>{

    @NotNull
    Session open(@Nullable String login, @Nullable String password);

    @Nullable
    User checkDataAccess(@Nullable String login, @Nullable String password);

    void validate(@Nullable Session session, Role role);

    void validate(@Nullable Session session);

    @NotNull
    Session sign(@NotNull Session session);

    void close(@Nullable Session session);

}
