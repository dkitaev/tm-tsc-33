package ru.tsc.kitaev.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kitaev.tm.model.Session;
import ru.tsc.kitaev.tm.model.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;

public interface IUserEndpoint {

    @Nullable
    @WebMethod
    User findById(
            @WebParam(name = "session", partName = "session") @Nullable Session session
    );

    @WebMethod
    void setPassword(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "password", partName = "password") @Nullable String password
    );

    @WebMethod
    void updateUser(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "firstName", partName = "firstName") @NotNull String firstName,
            @WebParam(name = "lastName", partName = "lastName") @NotNull String lastName,
            @WebParam(name = "middleName", partName = "middleName") @NotNull String middleName
    );

}
