package ru.tsc.kitaev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kitaev.tm.enumerated.Role;
import ru.tsc.kitaev.tm.model.User;

public interface IAuthService {

    @Nullable
    User getUser();

    @NotNull
    String getUserId();

    boolean isAuth();

    void logout();

    void login(@Nullable String login, @Nullable String password);

    void registry(@Nullable String login, @Nullable String password, @Nullable String email);

    void checkRoles(Role @Nullable...roles);

}
