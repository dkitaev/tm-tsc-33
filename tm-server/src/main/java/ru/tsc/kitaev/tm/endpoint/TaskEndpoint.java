package ru.tsc.kitaev.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kitaev.tm.api.endpoint.ITaskEndpoint;
import ru.tsc.kitaev.tm.api.service.IServiceLocator;
import ru.tsc.kitaev.tm.enumerated.Status;
import ru.tsc.kitaev.tm.exception.AbstractException;
import ru.tsc.kitaev.tm.model.Project;
import ru.tsc.kitaev.tm.model.Session;
import ru.tsc.kitaev.tm.model.Task;

import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
@NoArgsConstructor
public class TaskEndpoint extends AbstractEndpoint implements ITaskEndpoint {

    public TaskEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @Nullable
    public Task addTask(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "entity", partName = "entity") @Nullable Task entity
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().add(session.getUserId(), entity);
    }

    @Override
    public void removeTask(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "entity", partName = "entity") @Nullable Task entity
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getTaskService().remove(session.getUserId(), entity);
    }

    @Override
    public void clearTask(
            @WebParam(name = "session", partName = "session") @Nullable Session session
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getTaskService().clear(session.getUserId());
    }

    @Override
    @NotNull
    public List<Task> findAllTask(
            @WebParam(name = "session", partName = "session") @Nullable Session session
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().findAll(session.getUserId());
    }

    @Override
    @Nullable
    public Task findTaskById(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "id", partName = "id") @Nullable String id
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().findById(session.getUserId(), id);
    }

    @Override
    @NotNull
    public Task findTaskByIndex(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "index", partName = "index") @Nullable Integer index
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().findByIndex(session.getUserId(), index);
    }

    @Override
    @Nullable
    public Task removeTaskById(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "id", partName = "id") @Nullable String id
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().removeById(session.getUserId(), id);
    }

    @Override
    @Nullable
    public Task removeTaskByIndex(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "index", partName = "index") @Nullable Integer index
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().removeByIndex(session.getUserId(), index);
    }

    @Override
    @NotNull
    public Task createTask(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "name", partName = "name") @Nullable String name,
            @WebParam(name = "description", partName = "description") @Nullable String description
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().create(session.getUserId(), name, description);
    }

    @Override
    @NotNull
    public Task findTaskByName(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "name", partName = "name") @Nullable String name
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().findByName(session.getUserId(), name);
    }

    @Override
    @NotNull
    public Task removeTaskByName(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "name", partName = "name") @Nullable String name
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().removeByName(session.getUserId(), name);
    }

    @Override
    @Nullable
    public Task updateTaskById(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "id", partName = "id") @Nullable String id,
            @WebParam(name = "name", partName = "name") @Nullable String name,
            @WebParam(name = "description", partName = "description") @NotNull String description
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().updateById(session.getUserId(), id, name, description);
    }

    @Override
    @NotNull
    public Task updateTaskByIndex(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "index", partName = "index") @Nullable Integer index,
            @WebParam(name = "name", partName = "name") @Nullable String name,
            @WebParam(name = "description", partName = "description") @NotNull String description
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().updateByIndex(session.getUserId(), index, name, description);
    }

    @Override
    @Nullable
    public Task startTaskById(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "id", partName = "id") @Nullable String id
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().startById(session.getUserId(), id);
    }

    @Override
    @NotNull
    public Task startTaskByIndex(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "index", partName = "index") @Nullable Integer index
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().startByIndex(session.getUserId(), index);
    }

    @Override
    @NotNull
    public Task startTaskByName(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "name", partName = "name") @Nullable String name
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().startByName(session.getUserId(), name);
    }

    @Override
    @Nullable
    public Task finishTaskById(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "id", partName = "id") @Nullable String id
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().finishById(session.getUserId(), id);
    }

    @Override
    @NotNull
    public Task finishTaskByIndex(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "index", partName = "index") @Nullable Integer index
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().finishByIndex(session.getUserId(), index);
    }

    @Override
    @NotNull
    public Task finishTaskByName(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "name", partName = "name") @Nullable String name
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().finishByName(session.getUserId(), name);
    }

    @Override
    @Nullable
    public Task changeTaskStatusById(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "id", partName = "id") @Nullable String id,
            @WebParam(name = "status", partName = "status") @Nullable Status status
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().changeStatusById(session.getUserId(), id, status);
    }

    @Override
    @NotNull
    public Task changeTaskStatusByIndex(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "index", partName = "index") @Nullable Integer index,
            @WebParam(name = "status", partName = "status") @Nullable Status status
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().changeStatusByIndex(session.getUserId(), index, status);
    }

    @Override
    @NotNull
    public Task changeTaskStatusByName(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "name", partName = "name") @Nullable String name,
            @WebParam(name = "status", partName = "status") @Nullable Status status
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().changeStatusByName(session.getUserId(), name, status);
    }

}
